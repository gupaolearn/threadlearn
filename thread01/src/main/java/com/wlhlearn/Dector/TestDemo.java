package com.wlhlearn.Dector;

/**
 * @Author: wlh
 * @Date: 2019/5/29 9:07
 * @Version 1.0
 * @despricate:learn
 */
public class TestDemo {


    PrintProcess printProcess;

    public TestDemo() {
        SaveProcess saveProcess = new SaveProcess();
        saveProcess.start();
        this.printProcess = new PrintProcess(saveProcess);
        printProcess.start();
    }


    public void SetProcess(Request request) {
        printProcess.processRequest(request);
    }


    public static void main(String[] args) {

        Request request = new Request();
        request.setName("wlh");
        new TestDemo().SetProcess(request);

    }
}
