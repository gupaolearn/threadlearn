package com.wlhlearn.Dector;

/**
 * @Author: wlh
 * @Date: 2019/5/29 8:54
 * @Version 1.0
 * @despricate:责任链模式 实现一个请求的连续处理
 */
public interface IRequestPrecess {

    void  processRequest(Request request);
}
