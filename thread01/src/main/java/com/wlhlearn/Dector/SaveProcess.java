package com.wlhlearn.Dector;

import java.util.concurrent.LinkedBlockingDeque;

/**
 * @Author: wlh
 * @Date: 2019/5/29 9:03
 * @Version 1.0
 * @despricate:learn
 */
public class SaveProcess  extends Thread implements  IRequestPrecess {
    LinkedBlockingDeque<Request> requests=new LinkedBlockingDeque<>();

    private   IRequestPrecess requestPrecess ;

    public SaveProcess() {
    }

    public SaveProcess(IRequestPrecess requestPrecess) {
        this.requestPrecess = requestPrecess;
    }

    @Override
    public void processRequest(Request request) {
        requests.add(request);
    }


    @Override
    public void run() {

        while (true){
            try {
                Request request=requests.take();
                System.out.println("save process :"+request.getName());
                if(!(requestPrecess==null)){
                    requestPrecess.processRequest(request);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
