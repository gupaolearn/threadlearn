package com.wlhlearn.Dector;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @Author: wlh
 * @Date: 2019/5/29 8:54
 * @Version 1.0
 * @despricate:learn
 */
public class PrintProcess  extends Thread implements  IRequestPrecess {

    LinkedBlockingQueue<Request> requests=new LinkedBlockingQueue<Request>();

    private  final IRequestPrecess requestPrecess ;

    public PrintProcess(IRequestPrecess requestPrecess) {
        this.requestPrecess = requestPrecess;
    }

    @Override
    public void processRequest(Request request) {
        System.out.println("precess request");
        requests.add(request);
    }

    @Override
    public void run() {

        while (true){
            try
            {
                try {
                    Request request=requests.take() ;
                    System.out.println("print process:"+request.getName());
                    if(!(requestPrecess==null)){
                        requestPrecess.processRequest(request);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }catch (Exception e){

            }
        }
    }


}
