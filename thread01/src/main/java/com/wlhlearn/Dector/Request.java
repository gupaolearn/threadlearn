package com.wlhlearn.Dector;

/**
 * @Author: wlh
 * @Date: 2019/5/29 8:53
 * @Version 1.0
 * @despricate:learn
 */
public class Request {

    private  String name ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Request{" +
                "name='" + name + '\'' +
                '}';
    }
}
