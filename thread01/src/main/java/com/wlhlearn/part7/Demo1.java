package com.wlhlearn.part7;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: wlh
 * @Date: 2019/6/4 13:50
 * @Version 1.0
 * @despricate:learn
 */
public class Demo1  implements  Runnable {

    public static void main(String[] args) {
        ExecutorService service= Executors.newFixedThreadPool(2);

        for(int i=0;i<100 ;i++){
            service.execute(new Demo1());
        }
        service.shutdown();
    }

    @Override
    public void run() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName());

    }
}
