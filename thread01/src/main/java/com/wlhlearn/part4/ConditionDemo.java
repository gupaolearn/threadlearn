package com.wlhlearn.part4;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: wlh
 * @Date: 2019/6/6 15:57
 * @Version 1.0
 * @despricate:learn
 */
public class ConditionDemo {


    public static void main(String[] args) {
        Lock lock=new ReentrantLock();
       Condition condition=lock.newCondition();

       // 注意不同的顺序  执行的结果是完全不同的
       new Thread(new ConditionWaitDemo(lock,condition)).start();
       new Thread(new ConditionSignleDemo(lock,condition)).start();



    }
}
