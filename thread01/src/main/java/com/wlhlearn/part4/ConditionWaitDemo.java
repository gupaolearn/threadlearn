package com.wlhlearn.part4;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @Author: wlh
 * @Date: 2019/6/6 15:52
 * @Version 1.0
 * @despricate:learn
 */
public class ConditionWaitDemo  implements  Runnable {

   private Lock lock ;
   private Condition condition ;

    public ConditionWaitDemo(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {

        System.out.println("conditionwait begin");
        try{
            lock.lock();
            try {
                condition.await();
                System.out.println("conditionwait  end");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }finally {
            lock.unlock();
        }

    }
}
