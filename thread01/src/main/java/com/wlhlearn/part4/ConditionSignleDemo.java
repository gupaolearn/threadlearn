package com.wlhlearn.part4;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * @Author: wlh
 * @Date: 2019/6/6 15:55
 * @Version 1.0
 * @despricate:learn
 */
public class ConditionSignleDemo implements Runnable {

    private Lock lock ;

    private Condition condition ;

    public ConditionSignleDemo(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }

    @Override
    public void run() {
        System.out.println("conditionsingle begin");
        try {
            lock.lock();
            condition.signal();
            System.out.println("conditionsingle end");


        }finally {
             lock.unlock();
        }



    }
}
