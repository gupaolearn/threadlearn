package com.wlhlearn.part1;

import java.util.Date;

/**
 * @Author: wlh
 * @Date: 2019/6/5 14:35
 * @Version 1.0
 * @despricate: 责任链模式的实现 与使用
 *  模拟的场景：下单成功之后回传的信息 依次要经过 订单中心、支付中心、配送中心 次序的处理
 *
 */
public class TestDector {


    public static void main(String[] args) {

        OrderResponse response=new OrderResponse() ;
        response.setId(327283738L);
        response.setPayTime("2019-06-05");
        response.setPeisongDay(new Date());

        PayorderProcess payorderProcess=new PayorderProcess(null);
        SaleOrderProcess saleOrderProcess=new SaleOrderProcess(payorderProcess);
        saleOrderProcess.tryProcess(response);
        new Thread(payorderProcess).start();
        new Thread(saleOrderProcess).start();

    }

}
