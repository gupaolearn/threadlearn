package com.wlhlearn.part1;


import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wlh
 * @Date: 2019/6/5 14:39
 * @Version 1.0
 * @despricate:learn
 */
public class SaleOrderProcess    implements  DoOwnProcess,Runnable {

    List<OrderResponse> orderResponses=new ArrayList<OrderResponse>();
    private  final DoOwnProcess process ;

    public SaleOrderProcess(DoOwnProcess process) {
        this.process=process;
    }

    @Override
    public void tryProcess(OrderResponse orderResponse) {
        orderResponses.add(orderResponse);
    }


    @Override
    public void run() {
        OrderResponse orderResponse=orderResponses.get(0);
        if(orderResponse!=null){
            System.out.println("begin sale  order:"+orderResponse.getId());
            toDosomthing(orderResponse);
            // 传递给下一个责任链
            if(process!=null){
                process.tryProcess(orderResponse);
            }
        }
    }

    public void  toDosomthing(OrderResponse orderResponse){
        System.out.println("sale -----------------------------------");
    }

}
