package com.wlhlearn.part3;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Author: wlh
 * @Date: 2019/6/6 14:03
 * @Version 1.0
 * @despricate:learn
 */
public class LockDemo extends  Thread {

    static Map<String, Object> cacheMap = new HashMap<String, Object>();
    static ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    static Lock read = rwl.readLock();
    static Lock write = rwl.writeLock();


    public static final Object get(String key) {
        System.out.println("read begin");
        read.lock();
        try {
            return cacheMap.get(key);
        } finally {
            read.unlock();
        }
    }

    public static final void put(String key,Object o){
        write.lock();
        try{
            System.out.println("write begin");
            cacheMap.put(key,o);
        }finally {
            write.unlock();
        }
    }

    public static void main(String[] args) {
        LockDemo lockDemo=new LockDemo() ;

        for (int i=0;i<1000;i++){
            new Thread(lockDemo).start();
        }
    }


}
