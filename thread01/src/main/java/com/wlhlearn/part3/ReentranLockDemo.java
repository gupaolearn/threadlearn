package com.wlhlearn.part3;

/**
 * @Author: wlh
 * @Date: 2019/6/6 13:58
 * @Version 1.0
 * @despricate:
 * 比如调用 demo 方法获得了当前的对象锁，然后在这个方法中再去调用
demo2， demo2 中的存在同一个实例锁，这个时候当前线程会因为无法获得
demo2 的对象锁而阻塞，就会产生死锁。重入锁的设计目的是避免线程的死
锁。
 */
public class ReentranLockDemo {

    public  synchronized void  demo(){
        System.out.println("begin  demo");
        demo2();
    }

    public void  demo2(){
        System.out.println("begin demo2");
        synchronized (this){

        }
    }

    public static void main(String[] args) {
        ReentranLockDemo demo=new ReentranLockDemo();

        new Thread(demo::demo).start();
    }

}
