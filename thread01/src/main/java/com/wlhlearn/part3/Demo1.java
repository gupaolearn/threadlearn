package com.wlhlearn.part3;

/**
 * @Author: wlh
 * @Date: 2019/5/30 9:43
 * @Version 1.0
 * @despricate:learn
 */
public class Demo1 {

    private  static  boolean stop=false ;

    public static void main(String[] args) throws InterruptedException {


        Thread thread=new Thread(()->{
            int i=0 ;
            while (!stop){
                i++ ;
               System.out.println(i);
            }
        });
         thread.start();

         Thread.sleep(1000);
         stop=true;

    }
}
