package com.wlhlearn;

import java.util.concurrent.*;

/**
 * @Author: wlh
 * @Date: 2019/5/29 8:48
 * @Version 1.0
 * @despricate:learn
 */
public class CallableDemo  implements Callable<String> {
    @Override
    public String call() throws Exception {
        return "hello world";
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService executorService= Executors.newFixedThreadPool(1);

        CallableDemo callableDemo=new CallableDemo() ;

        Future<String> future=executorService.submit(callableDemo);

        System.out.println(future.get());

    }
}
