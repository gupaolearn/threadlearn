package com.wlhlearn.part2;

/**
 * @Author: wlh
 * @Date: 2019/6/5 16:04
 * @Version 1.0
 * @despricate:learn
 */
public class Demo1 {

    private  static  int count=0 ;


    public static  void inc(){
        synchronized (Demo1.class){
        try {

            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count++ ;
        System.out.println(count);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i=0 ;i<1000;i++){
            new Thread(()->Demo1.inc()).start();
        }
    }

}
