package com.wlhlearn;

import java.util.concurrent.TimeUnit;

/**
 * @Author: wlh
 * @Date: 2019/5/29 9:41
 * @Version 1.0
 * @despricate: 线程的中断方式
 *
 */
public class InterruptDemo {

    private  static  int i =0 ;

    public static void main(String[] args) throws InterruptedException {

          Thread thread=new Thread(()->{
              while (true){
                  if(Thread.currentThread().isInterrupted()){
                      System.out.println("before:"+Thread.currentThread().isInterrupted());
                      i++ ;
                     Thread.interrupted();
                      System.out.println("after:"+Thread.currentThread().isInterrupted());
                  }
              }
          },"interruptDemo");
          thread.start();

        TimeUnit.SECONDS.sleep(2);
        thread.interrupt();

    }
}
